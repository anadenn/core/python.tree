from core.strings import *

link_string="[ {0[name]} ]( /{0[tree_path]} )"
tag_string="[{0}](/lien/vers/tag/{0})"
link_list_string="- [ {0[name]} ]( /{0[tree_path]} )"

processor=Processor()
lines=TreeLineProcessor(parent=processor,text=link_list_string)
worlds=Worlds(parent=processor)
attrs=TreeAttrProcessor(parent=worlds,text=link_string)

test=StringTest(parent=worlds,test_function="startswith",text="@")
Replace(parent=test,keyword="@",text=tag_string)


def parse(node,string):
    global processor,lines,attrs
    processor.parent=node
    lines.set_selection([node,])
    attrs.set_selection([node,])
    return processor.process(string)
