from dataModel.models import *
from dataModel.structures.tree import NamedTree,TreeLoader
import os

#==========================================================

class Preload(Model,NamedTree,TreeLoader):
    
#==========================================================


    TEXT=String()
    RECURSIVE=Boolean(default=False)
    CLS=String(default="Data")

    DO=Function()

    #-----------------------------------------------------
    def do(self):

        filename=self.text%self.parent

        if not os.path.exists(filename):
            filename=os.path.join(os.environ["XMLPYTHON_PATH"],filename)

        if not os.path.exists(filename):
            return "ERROR no path "+filename

        return self.open(filename,self.parent,
                first=True,
                recursive=self.recursive,
                extention=".xml",
                cls=self.cls)
    #-----------------------------------------------------
#==========================================================
