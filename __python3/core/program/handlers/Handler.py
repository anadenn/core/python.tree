from dataModel.models import *
from ..Object import Object
from ..tree.Run import Run
from dataModel.structures.tree import TreeSelector


#==========================================================

class Handler(Run,TreeSelector):
    
#==========================================================
    """
    manipuler des arbres en sélectionnant des éléments qui seront
    transmis aux Handler enfants pour effectuer des opérations
    """

    PARENT_SELECTION=Boolean(default=True,doc="si False, ne va pas chercher la sélection parent")


   #---------------------------------------------------
    def onCall(self):
        """
        appel du noued, execution effective
        selection onSelect
        execution ononCallNode
        """
        yield self.onSelect()

        for elt in self.selection:
            yield self.onCallNode(elt)
        self.selection.clear()
   #---------------------------------------------------
    def onSelect(self):
        """
        créer la sélection pour l'execution
        par défault prend la sélection du parent
        surcharger pour avoir un comportement différent
        """
        if self.parent_selection==True:
            if self.parent and isinstance(self.parent,Handler):
                self.set_selection(self.parent.selection)

   #---------------------------------------------------
    def onCallNode(self,node):
        """
        pour chaque élément de la sélection, execute la fonction
        surcharger pour avoir un comportement
        """
        pass


   #---------------------------------------------------


#==========================================================

