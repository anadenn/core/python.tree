from dataModel.models import *
from ..Object import Object
from .Select import Select
from .Handler import Handler
from dataModel.Store import Store

#==========================================================

class Call(Select):
    
#==========================================================
    """
    permet d'appeler une selection de noeuds Run et de les executer

    """

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        execute le noeud spécifié
        """

        if isinstance(ressource,Handler):
            ressource.set_selection(self.selection)
        return ressource.call()
   #---------------------------------------------------
#==========================================================
