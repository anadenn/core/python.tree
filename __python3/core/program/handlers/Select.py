from dataModel.models import *
from ..Object import Object
from .Handler import Handler
from dataModel.Store import Store
from ..tree.Run import Run
from dataModel.structures.tree import TreeSelector

#==========================================================

class Select(Run,TreeSelector):
    
#==========================================================
    """
    permet de sélectionner des ressources grace à un descripteur
    dans la variable text

    """
   #---------------------------------------------------
    def onCall(self):
        """
        appel du noued, execution effective
        selection onSelect
        execution ononCallNode
        """
        yield self.onSelect()

        for elt in self.selection:
            yield self.onCallNode(elt)
        self.selection.clear()
   #---------------------------------------------------
    def onSelect(self):
        """
        crée la sélection à partir du descripteur
        par defaut, selectionne le descripteur
        fonction à surcharger pour avoir un comportement différent
        """

        self.set_selection(self.get_selection())

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        code à executer sur la sélection
        fonction à surcharger pour avoir un comportement
        """
        pass

   #---------------------------------------------------
#==========================================================
