from dataModel.models import *
from ..Object import Object
from ..tree.Run import Run
from .Module import Module
import console
#==========================================================

class Main(Run):
    
#==========================================================

    START_POINT=String(default="main")

    ASK=Function()

    #---------------------------------------------------
    def onPreSetup(self):
        self.modules=self.descendants.by_class(Module)

   #---------------------------------------------------


    def onPostSetup(self):
        pass
   #---------------------------------------------------


    def onPreCleanup(self):
        pass
    
   #---------------------------------------------------


    def onPostCleanup(self):
        pass

   #---------------------------------------------------
    def onCall(self):
        return self.find(self.start_point).call()

   #---------------------------------------------------
    def onCallChildren(self):
        pass

   #---------------------------------------------------
    def ask(self,url):
        pass
   #---------------------------------------------------
    def run(self):



        console.title("setup")
        console.add(self.get_records())
        console.add(self.stats())
        console.add(self.setup())

        console.title("call")
        console.add(self.stats())
        console.add(self.call())

        console.title("cleanup")
        console.add(self.stats())
        console.add(self.cleanup())






