import time,threading
from queue import Queue
from multiprocessing.dummy import Pool as pythonThreadPool

from .Run import Run
from dataModel.models import *

#===================================================================

class ThreadPool(Run):

#===================================================================
    """
    execution en parralèlle des enfants Run
    """


    N=Integer(default=4,doc="nombre de thread effectifs en meme temps")

    #----------------------------------------------------------------
    def onCall(self):
        """
        crée une liste de threads avec les enfants Run
        et attend la fin de l'execution pour renvoyer les résultats
        """
        process=pythonThreadPool(self.n)
        result=process.map(self.onThreadCall, self.children.by_class(Run))
        return result
    #----------------------------------------------------------------
    def onCallChildren(self):
        """
        bloque la propagation de call pour dans les descendants
        le thread executera dans sa boucle les descendants
        """
        pass

    #----------------------------------------------------------------
    def onThreadCall(self,node):
        """
        code d'execution effectif du modèle
        appelle pour chaque noeud node la fonction call
        surcharger pour avoir un comportement différent
        """
        return node.call()
#===================================================================

