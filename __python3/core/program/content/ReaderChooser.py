from dataModel.models import *
from core.program import Select,Relation
from core.data import Resource
from .Reader import Reader


#==============================================================

class ReaderChooser(Select):

#==============================================================
    """
    pour la sélection de ressources
    permet de trouver un lecteur Reader suivant la ressource
    et de lire la ressource sélectionnée
    """
    
    #---------------------------------------------------
    def onPostSetup(self):
        """
        bloque la recherche de selection dans les parents pour les Reader enfants 
            reader.parent_selection=False

        """
        for elt in self.children.by_class(Reader):
            elt.parent_selection=False
            elt.selection_passive=True
    #---------------------------------------------------
    def onCallNode(self,node):
        """
        pour chaque élément de la sélection
            recherche le lecteur Reader et lit la ressource

        """

        if isinstance(node,Resource) and node.exists() == True:

            reader=self.choose(node)
            if reader:

                reader.set_selection([node,])
                return reader.call()
            else:
                return "can't find format "+file_type+" for file "+res.disc_path

   #---------------------------------------------------
    def onCallChildren(self):
        """
        bloque la propagation de call dans les enfants
        """
        pass

    #-----------------------------------------------------
    def choose(self,node):


        for elt in self.children:
            if elt.test_res(node) ==True :
                return elt

        print("can't find format "+file_type+" for file "+res.text)



    #-----------------------------------------------------

#==============================================================
