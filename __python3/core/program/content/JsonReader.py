from dataModel.models import *
from core.data import Data
from .DictReader import DictReader
import json

#==============================================================
class JsonReader(DictReader):
#==============================================================
    """
    lecteur pour les textes bruts
    """
    #-----------------------------------------------------
    def onReadDict(self,content):
        """
        
        """
        return json.loads(content)

    #----------------------------------------------------------------


#==============================================================
