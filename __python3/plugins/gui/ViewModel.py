from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewOperation import ViewOperation

#======================================================

class ViewModel(ViewOperation):

#======================================================

    MODEL=String()

    def onSetArgs(self,data,output,**args):

        node_args=dict(self.getRecordDict())
        del node_args["name"]
        node = self.load_model(self.model)
        node.parent=output
        node.setup()

        return data,node,args


#======================================================

