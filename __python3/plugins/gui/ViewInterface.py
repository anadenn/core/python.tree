from dataModel.models import *
from core.program import *
from dataModel.Store import Store


#==========================================================

class ViewInterface(metaclass=ModelMeta):
    
#==========================================================

    """
    Une interface vue comme un arbre de Interface_Tool
    interface pour accéder aux ressource via le parent Interface_System
        - vues
        - parts
        - events
        - models

    """

    #-------------------------------------------------        
    def get_parent_view(self):
        """
        renvoie le systeme ou erreur si pas de parent systeme
        """
        for elt in self.ancestors:
            if isinstance(elt,ViewInterface):
                return elt
        print("no view ",self.path())
        raise Exception("no parent "+self.path())
    #-------------------------------------------------        
    def get_view(self,name):

        node = self.find(name)
        if node:
            return node

        try:
            return self.get_parent_view().get_view(name)
        except:
            raise Exception("no view "+name)

    #-------------------------------------------------        
    
    def call_view(self,name,data,output):
        view=self.get_view(name)
        if view:
            view.root_obj=output
            view.set_selection([data,])
            return view.call()
        else:
            return "ERROR no view "+name
    #-------------------------------------------------
    def load_model(self,filename):
        return self.get_system().load_model(filename)

    #-------------------------------------------------
    def get_part(self,part):

        node=None
        for elt in self.descendants:
            if elt.name==part:
                node = elt
                break

        if not node:
            try:
                view = self.get_parent_view()
                node= view.get_part(part)
            except:
                raise Exception("no part "+part)

        if node:
            return node
        else:
            print("no part",part,self.path())
            raise Exception("no part "+part)

    #-------------------------------------------------
    def event_message(self,**message):
        self.get_parent_view().event_message(**message)
    #-------------------------------------------------







#==========================================================
