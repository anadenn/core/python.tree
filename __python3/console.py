import time
import inspect,sys,traceback
from dataModel.Store import Store
import types

SIZE=80
SEPARATOR2="      | "
SEPARATOR ="        "
messages=[]

HIDE=[]
#HIDE=["GTK","MAKE"]#,"VIEW"]
#HIDE=["GTK","MAKE","VIEW"]
#===============================================================================
def add(node):
#===============================================================================
    global messages
    if type(node)==dict:

        #node["__time__"]=time.time()
        messages.append(node)
    elif type(node) is list and len(node)==0:
        pass
    else:
        messages.append(node)
    #show_data(node)
#===============================================================================
def line(char,prefix=None):
#===============================================================================
    print(prefix+char*SIZE)

#===============================================================================

def title(node,prefix="    |"):

#===============================================================================

    add("")
    add("_"*SIZE)
    add("")
    add(" "*10+node)
    add("_"*SIZE)
    add("")

#===============================================================================
def show():
#===============================================================================
    show_data(messages)


#===============================================================================
def error(**args):
#===============================================================================
    args["__error__"]=True
    add(**args)



#===============================================================================

def show_list(node,prefix=""):

#===============================================================================

    for elt in node:
        show_node(elt,prefix=prefix+"   ")

#===============================================================================

def show_model(node,prefix=""):

#===============================================================================

    show_node(node.get_records(),prefix=prefix)

    if hasattr(node,"children"):
        for elt in node.children:
            show_node(elt,prefix=prefix+SEPARATOR)


#===============================================================================

def show_dict_elt(k,node,prefix=""):

#===============================================================================




    if type(node) in [int,float,bool] or node is None:
        show_string(k+" : "+str(node),prefix=prefix)
    elif type(node) in [str,bytes]:

        if not type( node ) is str:
            node=node.decode('utf-8')

        if "\n" in node:
            show_string(k,prefix=prefix)
            show_multiline(node,prefix=prefix)
        else:
            show_string(k+" : "+node,prefix=prefix)

    else:
        show_string(k,prefix=prefix)
        show_node(node,prefix=prefix)

#===============================================================================

def show_dict(node,prefix=""):

#===============================================================================

    if len(node)>0:
        print(prefix+"    ")
    for k,v in node.items():

        show_dict_elt(k,v,prefix=prefix+SEPARATOR)
#===============================================================================

def show_multiline(node,prefix=""):

#===============================================================================

    for elt in node.split("\n"):  
        elt=elt.replace("\n","")
        show_string(elt,prefix=prefix+SEPARATOR2)

#===============================================================================

def show_string(node,prefix=""):

#===============================================================================
    node=str(node)

    for hide in HIDE:
        if node.startswith(hide):
            return

    if "\n" in node:
        show_multiline(node,prefix=prefix)
    else:
    #    if len(node)>100:
    #        node=node[:100]+"  [ ... ]"

        #if node.strip() !="":
        print(prefix,node)
#===============================================================================

def show_node(node,prefix=""):

#===============================================================================


    #if hasattr(node,"__show__"):

    #    node.__show__()


    if node is None:
        pass

    elif type( node ) in [str,bool,float,int]:
        show_string(node,prefix=prefix)

    elif type( node ) is bytes:
        show_string(node.decode('utf-8'),prefix=prefix)


    elif type(node) is list or isinstance(node,Store) or isinstance(node, types.GeneratorType):
        show_list(node,prefix=prefix)

    elif hasattr(node,"class_name"):
        show_model(node,prefix=prefix)

    elif type(node) is dict:
        show_dict(node,prefix=prefix)


    else:
        show_string(node,prefix=prefix)


#===============================================================================

def show_data(node,prefix="    |"):

#===============================================================================

    print()
    line("_",prefix=prefix)
    show_node(node,prefix=prefix)
    line("_",prefix=prefix)
    print()

#===============================================================================

def tree_stats(node,prefix="    |"):

#===============================================================================

    data=dict()
    data["descendants"]=len(node.descendants)
    data["leaves"]=len(node.leaves)
    data["deph"]=node.deph


    for i,level in node.getLevels():
        data["level_"+str(i)]=len(level)

    show_data(data)

#===============================================================================

def handle_errors(function):

#===============================================================================

    def execute(self,*args,**kwargs):


        try:
            return function(self,*args,**kwargs)
        except:

            exc_type, exc_value, exc_traceback = sys.exc_info()
            tb=traceback.extract_tb(exc_traceback )
            node=dict(exc_type=exc_type, exc_value=exc_value)

            
            if tb is not None:
                lst=[]
                for line in tb:
                    elt=dict(filename=line[0],line=line[1],function_name=line[2],text=line[3])
                    lst.append(elt)
                node["details"]=lst

            return node

    return execute



#===============================================================================
